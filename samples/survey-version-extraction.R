#
# Extração de versões existente em atividade
# utilizando autenticação por meio de token do projeto.
#
# TOKEN: Identificar único por usuário para extração.
#
# PROJECT: Identificador único do projeto.
# Presente na url do projeto, ex. otus-XXXX.otus-solutions.com.br
#
# ACRONYM: Sigla utilizada para identificar o instrumento desejado


library(otus.solutions)

otus <- OtusPlatform$new()

otus$api$authenticate(
  credentials = list(
    token = "TOKEN"
  ),
  project = "PROJECT"
)

versoes <- otus$api$extraction$activity$list_survey_versions("ACRONYM")
