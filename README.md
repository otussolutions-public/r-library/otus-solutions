# otus-solutions

Pacote [`R`](https://www.r-project.org/) para uso de serviços da Otus Solutions.

## Instalação e uso em R

Você pode instalar o pacote utilizando o comando `devtools::install_gitlab`, indicando o repositório público da Otus Solutions para busca do pacote;

```
devtools::install_gitlab(
  repo = "otussolutions-public/r-library/otus-solutions"
)
```

Para instalar a partir de *branches* de desenvolvimento, utilizar o trecho `@nome-da-branch` ao final da URL, como abaixo:

```
devtools::install_gitlab(
  repo = "otussolutions-public/r-library/otus-solutions@otus-dev"
)
```

Então, será possível utilizar as funcionalidades do pacote com o comando `library(otus.solutions)` ou o prefixo `otus.solutions::` antes de funções do pacote.

### Exemplo

O principal ponto de entrada para as funcionalidades do pacote é o objeto `OtusPlatform`, que pode ser instanciado e reutilizado durante o desenvolvimento de seus scripts e funções.

```
library(otus.solutions)

otus <- OtusPlatform$new()

# Autenticar com nome de usuário
otus$api$authenticate(
  credentials = list(
    email = "meu_email",
    password = "minha_senha"
  ),
  project = "meu_projeto"
)

# Autenticar com token de extração
otus$api$authenticate(
  credentials = list(
    token = "meu_token"
  ),
  project = "meu_projeto"
)

# Realizar extração
versoes_atividade <- otus$api$extraction$activity$list_survey_versions("ACRONIMO")
```

No exemplo acima:

- É criada uma instância da classe `OtusPlatform`
- Usa-se o objeto para autenticar o usuário na API pelo método `otus$api$authenticate()`
  - Este método consegue autenticar o usuário usando o **token de extração** ou a combinação de **e-mail e senha** do usuário
- Usa-se o mesmo objeto para acessar diferentes tipos de funcionalidades
  - Neste exemplo, foi realizada extração de dados referentes às versões disponíveis para determinada atividade, utilizando o método `otus$api$extraction$list_survey_versions("ACRONIMO")`

## Desenvolvimento

Os *scripts* R em `/inst/dev` são utilizados para desenvolvimento, onde:
- O arquivo `run_production.R` realiza todos procedimentos para publicação do pacote e deve ser rodado **toda vez que for publicada uma versão nova deste**
- O arquivo `run_dev.R` realiza procedimentos essenciais para utilização do pacote localmente (não aplica testes unitários nem conferências para publicação)
